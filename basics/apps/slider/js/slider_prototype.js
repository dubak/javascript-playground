
// define Slider class constructor
function Slider ( options, element ) 
{

	// class properties
  this.sliderWrap = '';
  this.elementName = '';
  
	// read options into class property
	this.customOptions = (typeof options == 'undefined') ? Slider.optionsDefault : options;

  // element is defined
  if ( typeof element !== 'undefined' && element !== '' )
  {        
    this.elementName = element;                            
  }
  else {
    alert ('Define element');
  }

	// class property
//	this.iWidth = options['width'];	
	
}



// class methods
Slider.prototype = {
  constructor: Slider,

  init: function() 
  {      
console.log(this.elementName);
    if ( typeof this.elementName !== 'undefined' && this.elementName !== '' && this.elementName !== null )
    {
      this.sliderWrap = document.getElementById(this.elementName);

      
      if ( typeof window.addEventListener !== 'undefined' ){
        this.sliderWrap.onmouseover = function(event) 
        {
            //console.log(event); 
            Slider.debug("you onmouseover " + event.target, 'log');
        };
        
        this.sliderWrap.addEventListener("click", function(event) {
  	       Slider.debug("you clicked " + event.target, 'log');
  	       event.preventDefault();
        });
      
      }
      // IE 8, 7
      else {
        this.sliderWrap.attachEvent("onclick", function(event) {
  	       Slider.debug("you clicked " + event, 'log');
  	       //event.preventDefault();
        });      
      }
      
                
      this.sliderWrap[window.addEventListener ? 'addEventListener' : 'attachEvent']('onmouseout', function(event) 
      {
        console.log(event);
      });
          
    }      

  },
  
  
/*
  bindMouseOut : function() {
  
  },
	
  
  bindMove : function() {
       
      //
  
  },
*/    

}


// default options property
Slider.optionsDefault = {
	bgColor: 'pink',	
	border: '1px solid red',
	width: '200px',
};

// class method
Slider.debug = function ( mLog, type ) 
{	
  if( window.console && window.console.log ) 
  {
    switch ( type ){
      case 'log':
        window.console.log( mLog );
      break;
      case 'debug':
        window.console.debug( mLog );
      break;  
    }  
  }  
};

