(function( $ ){

	$.fn.slideros = function( options ) {

		var defaults = {
			'border'       		: '1px solid green',
		    'backgroundColor' 	: 'blue'
		};
		
		// default options, extending them with any options that were provided
	    var settings = $.extend( {}, defaults, options);

		
		// return invoked jQuery object for chainability
	    return this.each(function() 
	    {
			// pass jQuery object to variable 
			var __this = $( this );
debug(this);
debug( __this );
debug( settings );
			__this.css({
				border: settings.border,
				'background-color': settings.backgroundColor,
				width: settings.width,
			});
	
	    });	
		
/*
		this.fadeOut('normal', function(){
console.log(this);
    	// the this keyword is a DOM element

  	})
*/				
	};

	// private function for debugging

	  function debug($obj) {
	    if (window.console && window.console.log)
	      //window.console.log('hilight selection count: ' + $obj.size());
	    window.console.log($obj);
	  };	

	  //console.log(  $('#sliderik').slideros()  );
	  	  
	  
})( jQuery );
