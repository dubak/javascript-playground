/**
 * @param element
 * @return void 
 */
function Calculator(element) {

    var _this = this;
    
    this.x = 0;
    this.y = 0;
    this.elOperand = {};
    
    this.bindButtons = function(){
        var ancestor = document.getElementById(element),
        descendents = ancestor.getElementsByTagName('button');
    
        for ( var i = 0; i < descendents.length; ++i) {
            var el = descendents[i];
            var atr = el.getAttribute('id');
            el.addEventListener( 'click', this.calculate );            
            el.typeParam = atr;
        }
    };
    
    this.bindOperand = function(){
        this.elOperand = document.getElementById('operand');
        this.elOperand.addEventListener( 'change', this.change );
    };

    this.change = function(){
//console.log('i am changed');
        this.x = _this.isNumeric(parseInt(document.getElementById('first').value)) ? parseInt(document.getElementById('first').value) : 0;
//console.log(this.x);        
        this.y = _this.isNumeric(parseInt(document.getElementById('second').value)) ? parseInt(document.getElementById('second').value) : 0;
//console.log(this.y);                        
        var result = document.getElementById('result');
        
        switch (_this.elOperand.value){
            case 'minus':
                result.innerHTML = this.x - this.y;
                break;
                
            case 'plus':
                result.innerHTML = this.x + this.y;
                break;
            
            case 'multiply':
                result.innerHTML = this.x * this.y;
                break;            
        }
    };

    this.calculate = function(event){
//console.log('calculate');
//console.log(event.target.typeParam );
        this.x = _this.isNumeric(parseInt(document.getElementById('first').value)) ? parseInt(document.getElementById('first').value) : 0;
//console.log(this.x);        
        this.y = _this.isNumeric(parseInt(document.getElementById('second').value)) ? parseInt(document.getElementById('second').value) : 0;
//console.log(this.y);                        
        var result = document.getElementById('result');
        
        switch (event.target.typeParam){
            case 'minus':
                result.innerHTML = this.x - this.y;
                break;
                
            case 'plus':
                result.innerHTML = this.x + this.y;
                break;

            case 'devide':
                result.innerHTML = this.x / this.y;
                break;            
            
            case 'multiply':
                result.innerHTML = this.x * this.y;
                break;            
                
            case 'sqrt':
                result.innerHTML = Math.sqrt(this.x);
                break;
        }
    };

    try {
        if ( typeof element === 'undefined' ){
            throw 'Error no element';
        }
        
        if ( element.length < 1 ){
            throw 'Error no element';
        }
        
        this.bindOperand();
        this.bindButtons();
        
    } catch (e) {
        //console.log(e instanceof Error); // true
        console.log(e.message);
    }
};

Calculator.prototype.isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
