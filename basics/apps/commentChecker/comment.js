
// wrapping $ sign into closure function => avoiding global scope conflict
(function($){
    
    $.fn.checkComment = function(opts)
    {        
        var words =  opts.words || [];
        var regex = RegExp(words.join('|'), 'gi');
        //var replacement = '<i>$&</i>';

        $(this).on('keyup', function(e)
        {
            var comment = $(this);
            var commentTxt = $(this).val();
            
            comment.val( commentTxt.replace(regex, "***") ); // replacement
        });

        return this;
    };

})(jQuery);


