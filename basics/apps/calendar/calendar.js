
// wrapping $ sign into closure function => avoiding global scope conflict
(function($){
    
    $.fn.renderCalendar = function(opts)
    {
        var _this = this;
        var months =  opts.months || { 2:'March' };
        var unixTimestamp = Date.now(); // in milliseconds
        var today = new Date();
        var weekdayName = $.fn.renderCalendar.getWeekdayName();
        var jCalendar = $(this);
        var result = '';
console.log('Date: '+today.getDate());        
console.log('Day: '+today.getDay());
        var weekNumber = Math.ceil( today.getDay() / 7 );
console.log('Week number: '+ weekNumber);        
        var month = today.getMonth();    
        result += '<div id="calendar-title">Today is: '+weekdayName+'/' + months[month]+'/'+today.getFullYear()+'</div>';
        var daysInMonth = new Date( today.getFullYear(), month+1, 0).getDate();
console.log('Days: '+daysInMonth);        
        var weeks = Math.ceil( daysInMonth / 7 );
console.log('Weeks: '+weeks);        
        result += '<table id="table"><tr>';
        result += $.fn.renderCalendar.getCalendarTable(daysInMonth, today);        
        result += '</tr></table>';
        result += '<div style="position:relative; top: 120px;"><span>Previous</span><span id="next">Next</span></div>';

        jCalendar.html( result );
                
        var calendarTable = this.find('#table');
console.log(calendarTable);                
        this.find('#next').on('click', function()
        {
            calendarTable.animate({
                left: '-200px' //-1*_this.innerWidth()
            }, 'slow', function() {
console.log('I am done');
                month += 1;
                daysInMonth = new Date( today.getFullYear(), month+1, 0).getDate();
console.log(daysInMonth);                
                calendarTable.html('');
                var newMonth = '';
                newMonth += '<table id="table"><tr>';
                newMonth += $.fn.renderCalendar.getCalendarTable(daysInMonth, today);        
                newMonth += '</tr></table>';
            
                calendarTable.html(newMonth).animate({
                    left: 0
                }, 'slow');                
            });
        });
        
        return this;
    };

    $.fn.renderCalendar.getWeekdayName = function() {
        var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        return weekday[new Date().getDay()];
    };

    $.fn.renderCalendar.getCalendarTable = function(daysInMonth, today) 
    {
        var result = '';
        
        for (var i = 1; i <= daysInMonth; i++) 
        {
            result += '<td class="'+( (today.getDate() === i) ? 'currentDay' : '')+'">'+i+'</td>';
            
            if ( i % 7 === 0 )
            {    
                result += '</tr><tr>';
            }            
        }
        
        return result;
    };

})(jQuery);


