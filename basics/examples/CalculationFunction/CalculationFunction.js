/**
 * @param void
 * @return void 
 */
function Calculation () {
  //this.hello = 'world';
  
  this.x = 0;
  
  this.y = 0;
  
  var i = 0;
  
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
    
  this.square = function (pa_i){
     i = pa_i * pa_i;
     return i;
  }
  
  this.multiply = function (pa_x, pa_y){
  
    if ( isNumeric(pa_x) && isNumeric(pa_y) )
    {
       this.x = pa_x;
       this.y = pa_y; 
       return 'Result is: ' + this.x * this.y;
    }
    else {
       console.log('Error'); 
    }
    
  }
  
  //this.
  
};

var oInstance = new Calculation(); //.bind({ hello: 'world' });

console.log( oInstance );
console.log( oInstance.hello );
console.log( oInstance.square(3) );
console.log( oInstance.multiply(4, 3) );
console.log( oInstance.x + ' ' + oInstance.y );
//console.log( b.isNumeric(4) ); // not a function
