/**
 * Manages whole user interface
 *
 * @param {DOM Object container} container User interface container    
 * @param {function successCallback} callback Function to call when adding/editing/deleting was successfull
 */
function ClientInterface (container, options, callback)
{
    var _this = this;

    /**
     * Container for user interface
     *
     * @var DOM Object
     */
    _this.container = container;
 
    /**
     * User options
     *
     * @var Object
     */
    _this.options = options;
    
    /**
     * Function to call when adding/editing/deleting was successfull
     *
     * @var function
     */
    _this.callback = callback;

    
    /**
     * Initializes user interface
     */
    _this.init = function ()
    {
        if ( typeof _this.container === 'undefined' || _this.container.length === 0 ) 
        {            
            _this.callback({ state:false, message: "Could not initialize filter's interface!" });
            return;
        }

        _this.initDelete();
        //_this.initEdit();
    };    
    

    /**
     * Initializes user delete
     */
    _this.initDelete = function ( )
    {
        // delete client in list
        $(_this.container).find("[data-clients-list] [data-client-item] [data-delete-button]").click(function(e)
        {
            e.preventDefault();
            var clientID = $(this).closest('[data-client-item]').attr("data-id");  
            _this.deleteClient(clientID, _this.options.apiUrl);
        });   
    };
    
    
    /**
     * Deletes client with ID
     *
     * @param {int} clientID ID of client to delete
     */
    _this.deleteClient = function(clientID, apiURL)
    {
        var result = confirm("Do you want to delete client?");
        
        if (result){
            if (result) {
                $.post( apiURL + "deleteClient", { data: {clientID:clientID} }, function(data)
                {
                    var response = null;
                    console.log(data);
                    try {
                        response = $.parseJSON(data);
                    }
                    catch(ex) {
                        _this.callback({ state:false, message: "Could not delete client!" });
                        //bootbox.alert("Nepodarilo sa vymazať klienta");
                        return;
                    }
                    console.log(response);
                    if(response === 'undefined' || response === null || response.result !== 1)
                    {   
                        _this.callback({ state:false, message: "Could not delete client!" });
                        //bootbox.alert("Nepodarilo sa vymazať klienta");
                        return;
                    }
                    else {
                        _this.callback({ state:true, message: "Client deleted!" });
                        return;                        
                    }

                    //window.location.reload();
                }).fail( function(){
                    _this.callback({ state:false, message: "Could not delete client!" });
                    return;                    
                });
            }
        }
    };
    
}