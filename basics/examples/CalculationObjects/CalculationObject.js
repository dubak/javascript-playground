
Object = {

  x: 1,
  
  y: 0,

  init: function(){
  
    document.write('Initializing at: ' + window.location );
  
  },

  square: function (pa_i){
     i =  Math.pow(pa_i, 2);
     return 'Square of ' + pa_i + ' is: ' + i;
  },

  isNumeric: function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  },
    
  multiply: function( pa_x, pa_y ){
    console.log( this.x );
    var result = false;
      
    if ( this.isNumeric(pa_x) && this.isNumeric(pa_y) )
    {
      this.x = pa_x;
      this.y = pa_y;
      
      result = 'Result is: ' + this.x * this.y;    
    }
    
    return result;  
  },
  
};

console.log( Object );
console.log( Object.square(8) );
console.log( Object.multiply("8", 7) );
